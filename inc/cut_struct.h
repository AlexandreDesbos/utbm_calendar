#ifndef CUT_STRUCT_H_INCLUDED
#define CUT_STRUCT_H_INCLUDED

#include "inc_structure.h"

#define TAILLE_LIGNES 150
#define NOMBRE_LIGNES 35
#define NOMBRE_COURS 20

void cut_struct(cours [NOMBRE_COURS], char [NOMBRE_LIGNES][TAILLE_LIGNES]);

#endif // CUT_STRUCT_H_INCLUDED
