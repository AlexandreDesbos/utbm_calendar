#ifndef INC_STRUCTURE_H_INCLUDED
#define INC_STRUCTURE_H_INCLUDED

//declaration des structure
struct cours{
   char UV[5];
   char group[4];
   char day[8]; //a voir pour formater en nombre dans le formatage
   char startHour[5];//heure de début de l'event
   char endHour[5];//heure de fin de l'event
   char frequency[1];
   char room[10];
};
typedef struct cours cours;

//déclaration énumération
enum moment
{
START, END
};
typedef enum moment moment;

//https://extranet1.utbm.fr/gpedago/dossier/edt.xhtml?etudNumero
#endif
