/*cette fonction decoupe les chaines de caractere en fonction des parties de la
structure (heure, salle..) et les associe aux structures */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "../inc/struct_fill.h"
#include "../inc/inc_structure.h"

#define NOMBRE_COURS 18
#define TAILLE_LIGNES 150
#define NOMBRE_LIGNES 35

#define AFFICHER_EVEN 0


//remplissage des UV et des groupes dans les structures cours
void cut_struct(cours courses[NOMBRE_COURS], char raw_courses_lines[NOMBRE_LIGNES][TAILLE_LIGNES]){

    char weekday[][10] = {"lundi","mardi","mercredi", "jeudi","vendredi","samedi"}; //permet de modifier les jours en cas de changement de langue
   	short int j; //compteur boucle

    for(j = 0 ; j < NOMBRE_COURS ; j++){

      short int k; //curseur
      //short int *pk;
      //pk = &k;
      k = 0;

      //-------------------------------------------- UV --------------------------------------------

      strncpy(courses[j].UV, &raw_courses_lines[j][0],4); //copie les 4 premiers caracteres
      if(AFFICHER_EVEN==1) fprintf(stdout, "UV : %s \n",courses[j].UV);

      //-------------------------------------------- Group -----------------------------------------

      //Dans le cas des cours ne disposant pas de groupe spécifié

      if(isspace(raw_courses_lines[j][4]) !=0 && isspace(raw_courses_lines[j][5]) !=0){

         //copie du group
         strncpy(courses[j].group, &raw_courses_lines[j][6],2);
         if(AFFICHER_EVEN==1) fprintf(stdout, "group : %s \n",courses[j].group); //debug
         k = 11;

      }

      //Dans le cas des autres cours
      else{

         strncpy(courses[j].group, &raw_courses_lines[j][7],2);
         if(AFFICHER_EVEN==1) fprintf(stdout, "group : %s \n",courses[j].group);
         k = 12;
      }

      char *test = strstr(raw_courses_lines[j],weekday[0]);

      //------------------------------------------- Monday  -------------------------------------------
      if (test != NULL){

		  strcpy(courses[j].day,"MO");
	    if(AFFICHER_EVEN==1) fprintf(stdout,"jour = %s \n", courses[j].day);
      struct_fill(k, j,  courses , raw_courses_lines);

        }

	    else{

		    char *test = strstr(raw_courses_lines[j],weekday[1]);

		    //------------------------------------------- Tuesday -------------------------------------------

        if(test != NULL){

           strcpy(courses[j].day,"TU");
		       if(AFFICHER_EVEN==1) fprintf(stdout,"jour = %s \n", courses[j].day);
           struct_fill(k, j,  courses , raw_courses_lines);

        }else{

		      char *test = strstr(raw_courses_lines[j],weekday[2]);

		      //------------------------------------------- Wednesday -------------------------------------------
		      if(test != NULL){

            strcpy(courses[j].day,"WE");
            if(AFFICHER_EVEN==1) fprintf(stdout,"jour = %s \n", courses[j].day);
            struct_fill(k, j,  courses , raw_courses_lines);

		     	}else{

		         char *test = strstr(raw_courses_lines[j],weekday[3]);

		         //=------------------------------------------- Thursday -------------------------------------------
		         if(test != NULL){

		            strcpy(courses[j].day,"TH");
		            if(AFFICHER_EVEN==1) fprintf(stdout,"jour = %s \n", courses[j].day);
                struct_fill(k, j,  courses , raw_courses_lines);

                 }else{

                  char *test = strstr(raw_courses_lines[j],weekday[4]);

                  //------------------------------------------- Friday --------------------------------------------
                  if(test != NULL){
                   strcpy(courses[j].day,"FR");
                   if(AFFICHER_EVEN==1) fprintf(stdout,"jour = %s \n", courses[j].day);
                   struct_fill(k, j,  courses , raw_courses_lines);

                  }else{

                      char *test = strstr(raw_courses_lines[j],weekday[5]);
                      //-------------------------------------------- Saturday --------------------------------------------
                      if(test != NULL){

                      strcpy(courses[j].day,"SA");
                      if(AFFICHER_EVEN==1) fprintf(stdout,"jour = %s \n", courses[j].day);
                      struct_fill(k, j,  courses , raw_courses_lines);

                      }
		            }
		         }
		       }
		    }
        }



   }
   if(AFFICHER_EVEN==1) system("pause");
}
