#include <stdio.h>
#include <stdlib.h>
//fprintf dans le fichier ICS son en-tête
void ics_head(FILE* ICS)
{
fprintf(ICS,"BEGIN:VCALENDAR\n");
fprintf(ICS,"PRODID:-//UTBM//Utéboéhémien//Flavien Vargues, Alexandre Desbos & Mathis Feron//FR\n");
fprintf(ICS,"VERSION:2.0\n\n");//mémo rfc5445

fprintf(ICS,"BEGIN:VTIMEZONE\n");
fprintf(ICS,"TZID:Europe/Paris\n");
fprintf(ICS,"BEGIN:DAYLIGHT\n");
fprintf(ICS,"TZOFFSETFROM:+0100\n");
fprintf(ICS,"TZOFFSETTO:+0200\n");
fprintf(ICS,"DTSTART:19700329T020000\n");
fprintf(ICS,"RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\n");
fprintf(ICS,"END:DAYLIGHT\n\n");

fprintf(ICS,"BEGIN:STANDARD\n");
fprintf(ICS,"TZOFFSETFROM:+0200\n");
fprintf(ICS,"TZOFFSETTO:+0100\n");
fprintf(ICS,"DTSTART:19701025T030000\n");
fprintf(ICS,"RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\n");
fprintf(ICS,"END:STANDARD\n");
fprintf(ICS,"END:VTIMEZONE\n\n");
}
