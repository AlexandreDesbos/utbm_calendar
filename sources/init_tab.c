#include <stdio.h>
#include <stdlib.h>

#include "../inc/init_tab.h"
#include "../inc/inc_structure.h"

#define NOMBRE_COURS 20

//
void init_tab(cours courses[]){

    for(int i = 0; i < NOMBRE_COURS; i++){

        for(int j = 0; j < 5; j ++)
        {
            courses[i].UV[j] = '\0';
        }
        for(int j = 0 ; j < 9; j ++)
        {
            courses[i].day[j] = '\0';
        }
        for(int j = 0 ; j < 5; j ++)
        {
            courses[i].group[j] = '\0';
        }
        for(int j = 0 ; j < 6; j ++)
        {
            courses[i].startHour[j] = '\0';
        }
        for(int j = 0; j < 6; j ++)
        {
            courses[i].endHour[j] = '\0';
        }
        for(int j = 0; j < 11; j ++)
        {
            courses[i].room[j] = '\0';
        }
        courses[i].frequency[0]='\0';
    }

}
