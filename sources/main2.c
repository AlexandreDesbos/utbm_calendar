/*------------------------------------------------------------
UTBM_calendar

Copyright (©)  Alexandre DESBOS, Flavien VARGUES, Mathis FERON 2019

License : GNU GPL v3

Project link : https://framagit.org/barbecueparty/utbm_calendar

------------------------------------------------------------
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../inc/inc_structure.h"
#include "../inc/ics_head.h"
#include "../inc/special_events_acquisition.h"
#include "../inc/event_generator.h"
#include "../inc/web_scraping.h"
#include "../inc/raw_input_reset.h"
#include "../inc/text_format.h"
#include "../inc/cut_struct.h"
#include "../inc/init_tab.h"

#define n_ligne 50
#define TAILLE_LIGNES 150
#define NOMBRE_LIGNES 35
#define NOMBRE_COURS 20
#define ICS_CONSOLE 0


int main(void)
{

    //Declaration variables et tableaux
    int exceptions[n_ligne];
        for(int i=0;i<n_ligne+1;i++) exceptions[i]=-1;

    char raw_courses_lines[NOMBRE_COURS][TAILLE_LIGNES];
    cours courses[NOMBRE_COURS];

    //ouverture du fichier ICS
    FILE* ICS=NULL;
    ICS=fopen("EDT.ics","w+");
        if(ICS_CONSOLE==1) ICS=stdout;


    raw_input_reset();

    printf("Bienvenue dans l'UTBM Calendar\n");
    printf("N'oubliez pas de lire le readme si vous n'avez jamais utilise ce programme !\n");
    printf("Ce logiciel vous permettra de recuperer votre emploi du temps sur votre espace UTBM et vous creera un fichier ICS\n");
    printf("Ce fichier ICS pourra etre importe sur l'appli calendrier de votre choix (Google Calendar, I calendar, zimbra etc...)\n\n");
    printf("Veuillez vous munir du pdf calendrier universitaire de cette annee pour pouvoir entrer les dates de vacances, etc...\n");
    printf("Un bloc note avec des instructions ainsi que votre page emploi du temps sur votre espace UTBM vont s'ouvrir\n");
    printf("Connectez vous sur la page web ouverte et suivez les instructions du bloc note, revenez ensuite sur cette console.\n");
    system("pause");

    web_scraping();
    init_tab(courses);
    text_format(raw_courses_lines);
    cut_struct(courses,raw_courses_lines);
    special_events_acquisition(exceptions);
    ics_head(ICS);
    event_generator(ICS,courses,exceptions);


    printf("Votre fichier ICS a ete genere avec succes!\n");
    printf("Vous pouver le recuperer dans le dossier du programme sous le nom: EDT.ics\n");
    printf("Le fichier sera ecrase a la prochaine utilisation du programme\n\n");
    printf("Vous pouvez maintenant importer votre fichier sur votre appli calendrier preferee!");

    return EXIT_SUCCESS;
}
