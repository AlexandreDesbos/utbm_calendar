#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "../inc/inc_structure.h"

#define NOMBRE_COURS 18
#define TAILLE_LIGNES 150
#define NOMBRE_LIGNES 35

#define AFFICHER_EVEN 0

//remplit les structures avec les parties d�coup�es de raw_courses_lignes
void struct_fill(short int k, short int j, cours courses[NOMBRE_COURS], char raw_courses_lines[NOMBRE_LIGNES][TAILLE_LIGNES] ){


    char Temp[5];
    for(int v = 0; v < 6; v++){  //memset()
        Temp[v] = '\0';
    }

    	while(isspace(raw_courses_lines[j][k]) == 0){
        	k = k+ 1;
        }
   		   //startHour

        strncpy(Temp, &raw_courses_lines[j][k+1],5);

        if(Temp[4] == '\t'){ //cas l'heure n'est qu'un chiffre

          Temp[4] = '\0';  //on remplace le \t par un \0
          strcpy(&Temp[1], &Temp[2]); //on supprime les 2 points
          courses[j].startHour[0] = '0'; // on ajoute un 0 au debut du la chaine de caractere
          strcat(courses[j].startHour,Temp); //on copie la chaine temp dans la structure

          if(AFFICHER_EVEN==1) fprintf(stdout,"startHour = %s\n",courses[j].startHour);

        }
        else{ //cas ou l'heure est "normale"

          strcpy(&Temp[2], &Temp[3]); //on supprime les 2 points
          strcat(courses[j].startHour,Temp); //
          if(AFFICHER_EVEN==1) printf("startHour = %s\n",courses[j].startHour);

        }
        k = k + 1;
        while(isspace(raw_courses_lines[j][k]) == 0){
            k = k + 1;
        }

        //endHour
        //strncpy(courses[j].endHour, &raw_courses_lines[j][*pk+1],6);

        for(int i = 0; i < 5; i++){  //memset()
            Temp[i] = '\0';
        }

        strncpy(Temp, &raw_courses_lines[j][k+1],5);

        if(Temp[4] == '\t'){ //cas l'heure n'est qu'un chiffre

          Temp[4] = '\0';                          //on remplace le \t par un \0
          strcpy(&Temp[1], &Temp[2]);     //on supprime les 2 points
          courses[j].endHour[0] = '0';                      // on ajoute un 0 au debut du la chaine de caractere
          strcat(courses[j].endHour,Temp);         //on copie la chaine temp dans la structure

          if(AFFICHER_EVEN==1) fprintf(stdout,"endHour = %s\n",courses[j].endHour);

        }
        else{ //cas ou l'heure est "normale"
          strcpy(&Temp[2], &Temp[3]); //on supprime les 2 points
          strcat(courses[j].endHour,Temp); //
          if(AFFICHER_EVEN==1) printf("endHour = %s\n",courses[j].endHour);

        }
        k = k + 1;
        while(isspace(raw_courses_lines[j][k]) == 0){
            k = k + 1;
        }
        //frequency
        strncpy(courses[j].frequency, &raw_courses_lines[j][k+1],1);
        if(AFFICHER_EVEN==1) fprintf(stdout, "frequency : %s\n",courses[j].frequency);

        k = k+ 1;
        while(isspace(raw_courses_lines[j][k]) == 0){
            k = k+ 1;
        }
        //room
        strncpy(courses[j].room, &raw_courses_lines[j][k+1],10);

        short int l = 3;
        while(isspace(courses[j].room[l]) ==0){
            l++;
        }
        strcpy(&courses[j].room[l], &courses[j].room[l+1]);


        if(AFFICHER_EVEN==1) fprintf(stdout,"room : %s \n",courses[j].room);
}


//add \0
