#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define TAILLE_LIGNES 150
#define NOMBRE_LIGNES 48
#define NOMBRE_COURS 20

//Cette fonction recupere les lignes du fichier raw_input.txt et places seulement les lignes utiles (cours)
//dans le tableau raw_courses_lines

void text_format(char raw_courses_lines[NOMBRE_COURS][TAILLE_LIGNES])
{
    char line[TAILLE_LIGNES];  //chaine de caractere temporaire contenant la ligne en cours de lecture
    char raw_text_lines[NOMBRE_LIGNES][TAILLE_LIGNES];//tableau de chaines de caractere contenant l'ensemble des lignes du fichier raw_input.txt
    int compteur_ligne = 0; //compteur permettant de placer la ligne temporaire dans la case associee du tableau raw_text_lines
    int compteur_cours = 29; //compteur permettant de placer les lignes de raw_text_lines dans raw_courses_lines à partir de la ligne 30: premiere ligne de cours

    FILE* fichier = NULL;
    fichier = fopen("files/raw_input.txt", "r"); //ouverture fichier raw_input.txt en mode lecture

    if(fichier == NULL)
    {
        printf("Fichier non trouve \n"); //erreur si fichier non trouve
    }
    else //si fichier trouve:
    {

        while(fgets(line, TAILLE_LIGNES, fichier)!=NULL) //tant qu'il y a des lignes dans le fichier, les stocker dans line
        {
            strcpy(raw_text_lines[compteur_ligne], line); //copier la ligne dans la case correspondante du tableau raw_text_lines
            compteur_ligne++; //passer à la ligne suivante
        }

        for(int j = 0; j<NOMBRE_COURS;j++)
        {
            strcpy(raw_courses_lines[j], raw_text_lines[compteur_cours]); //copier la 30 eme ligne de raw_text_lines dans la première de raw_courses_lines etc pour avoir seulement les lignes nescessaires
            compteur_cours++; //passer au cours suivant
        }

        fclose(fichier);
    }
}
