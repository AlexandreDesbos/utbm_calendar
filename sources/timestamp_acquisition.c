#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../inc/assess_timestamp.h"

#define TAILLE_TEMP 8

/** \type de timestamp
 * -1: non test�
 * 0 : refus� longueur interdite
 * 3 : short timestamp AAAAMMJJ
 * 4 : incoh�rence de date : avant date de d�but
 * 5 : incoh�rence de date : pas dans temps de cours
 * 6 : incoh�rence de date : date mal �crite
 */

 //charg� de l'�xecution de la saisie et de sa v�rification avant de la mettre dans le tableau
void timestamp_acquisition(int exceptions[], int n)
{
    char temp[TAILLE_TEMP];
    int valid=-1;

    do
    {
        if(valid==0) printf("Format invalide. Ressayez :");
        if(valid==4) printf("Incoherence de dates. Date avant le debut de l'evenement. Ressayez :");
        if(valid==5) printf("Incoherence de dates. Pas dans l'intervalle de cours indique avant. Ressayez :");
        if(valid==6) printf("Incoherence de dates. Date mal ecrite. Ressayez au format AAAAMMJJ :");

        //gets(temp);
        fgets(temp, 9, stdin);
        temp[strcspn(temp, "\n")] = '\0';
        fflush(stdin);


        valid=assess_timestamp(temp,n,exceptions);
    }while(valid!=3);

    exceptions[n]=atoi(temp);//copie en int du string saisie
}
