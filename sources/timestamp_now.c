#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//met dans le string now un timestamp du moment de la génération de l'ICS
void timestamp_now(char now[])
{
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    strftime(now,sizeof("AAAAMMJJTHHMMSSZ"),"%Y%m%dT%H%M%SZ",tm);
}
