#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "../inc/decompose_date.h"

#define TAILLE_EXDATE 200

//remplit un tableau des dates entre deux dates en paramètres et renvoie le nombre de jours entre ces dates
int timestamp_range_generator(long everydates[], long date1, long date2)
{
    long date_format1;
    int nbjour, i=0;
    struct tm d1 = {0};
    struct tm d2 = {0};
    int date1_yyyymmdd[8];
    int date2_yyyymmdd[8];

    //initialise le tableau everydates à 0
    for(int i=0;i<TAILLE_EXDATE;i++)
    {
        everydates[i]=-1;
    }

    //decompose la date YYYYMMDD (long) par chiffres dans un tableau
    decompose_date(date1, date1_yyyymmdd);
    decompose_date(date2, date2_yyyymmdd);

    //place les jours, mois et années dans la structure tm de time.h
    d1.tm_mday = date1_yyyymmdd[0]+10*date1_yyyymmdd[1];
    d1.tm_mon = date1_yyyymmdd[2]+10*date1_yyyymmdd[3]-1;
    d1.tm_year = date1_yyyymmdd[4]+10*date1_yyyymmdd[5]+100*date1_yyyymmdd[6]+1000*date1_yyyymmdd[7]-1900;

    d2.tm_mday = date2_yyyymmdd[0]+10*date2_yyyymmdd[1];
    d2.tm_mon = date2_yyyymmdd[2]+10*date2_yyyymmdd[3]-1;
    d2.tm_year = date2_yyyymmdd[4]+10*date2_yyyymmdd[5]+100*date2_yyyymmdd[6]+1000*date2_yyyymmdd[7]-1900;

    //calcule le nombre de jours entre les deux dates
    nbjour = difftime(mktime(&d2),mktime(&d1))/86400; //La division par 86400 permet de convertir la valeur rendue par difftime de secondes en jours

    d1.tm_mday = date1_yyyymmdd[0]+10*date1_yyyymmdd[1];
    d1.tm_mon = date1_yyyymmdd[2]+10*date1_yyyymmdd[3];
    d1.tm_year = date1_yyyymmdd[4]+10*date1_yyyymmdd[5]+100*date1_yyyymmdd[6]+1000*date1_yyyymmdd[7];

    do
    {
        date_format1 = date1_yyyymmdd[7]*10000000+date1_yyyymmdd[6]*1000000+date1_yyyymmdd[5]*100000+date1_yyyymmdd[4]*10000+date1_yyyymmdd[3]*1000+date1_yyyymmdd[2]*100+date1_yyyymmdd[1]*10+date1_yyyymmdd[0];
        //date_format2 = date2_yyyymmdd[7]*10000000+date2_yyyymmdd[6]*1000000+date2_yyyymmdd[5]*100000+date2_yyyymmdd[4]*10000+date2_yyyymmdd[3]*1000+date2_yyyymmdd[2]*100+date2_yyyymmdd[1]*10+date2_yyyymmdd[0];

        everydates[i]=date_format1;

        if(d1.tm_mon%2!=0) // Si mois de 30 jours
        {
            if(d1.tm_mday<30)
            {
                d1.tm_mday++;
            }
            else
            {
                d1.tm_mday=1;
                if(d1.tm_mon < 12)
                {
                    d1.tm_mon++;
                }
                else
                {
                    d1.tm_mon = 1;
                    d1.tm_year++;
                }
            }
        }
        else
        {
            if(d1.tm_mon==2)  //si mois de fevrier
            {
                if(d1.tm_mday<28)
                {
                    d1.tm_mday++;
                }
                else
                {
                    d1.tm_mday=1;
                    if(d1.tm_mon < 12)
                    {
                        d1.tm_mon++;
                    }
                    else
                    {
                        d1.tm_mon = 1;
                        d1.tm_year++;
                    }
                }
            }
            else                            //si mois de 31 jours
            {
                if(d1.tm_mday<31)
                {
                    d1.tm_mday++;
                }
                else
                {
                    d1.tm_mday=1;
                    if(d1.tm_mon < 12)
                    {
                        d1.tm_mon++;
                    }
                    else
                    {
                        d1.tm_mon = 1;
                        d1.tm_year++;
                    }
                }
            }
        }
        date1_yyyymmdd[0]=d1.tm_mday%10;
        date1_yyyymmdd[1]=(d1.tm_mday-date1_yyyymmdd[0])/10;
        date1_yyyymmdd[2]=d1.tm_mon%10;
        date1_yyyymmdd[3]=(d1.tm_mon-date1_yyyymmdd[2])/10;
        date1_yyyymmdd[4]=d1.tm_year%10;
        date1_yyyymmdd[5]=((d1.tm_year-date1_yyyymmdd[4])/10)%10;
        date1_yyyymmdd[6]=(d1.tm_year-10*date1_yyyymmdd[5]-date1_yyyymmdd[4])%10;
        date1_yyyymmdd[7]= (d1.tm_year-100*date1_yyyymmdd[6]-10*date1_yyyymmdd[5]-date1_yyyymmdd[4])/1000;
        i++;
    }while(date_format1!=date2);


    return nbjour;
}
